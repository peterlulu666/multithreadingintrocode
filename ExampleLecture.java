public class ExampleLecture {
    static Integer x = 0;
    static Integer y = 0;
    static Integer z = 0;

    public static void main(String[] args) {
        Thread t1 = new Thread() {
            public void run() {
                System.out.println("Thread 1 is running.");
                x = y + z;
                System.out.println("x = " + x);
            }
        };
        Thread t2 = new Thread() {
            public void run() {
                System.out.println("Thread 2 is running.");
                y = 1;
                z = 2;
            }
        };
        // start both threads
        t1.start();
        t2.start();
    }
};