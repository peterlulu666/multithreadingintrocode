public class DaemonExample {
    public static void main(String[] args) throws InterruptedException {
        final Thread main_thread = Thread.currentThread();
        Thread child_thread = new Thread() {
            public void run() {
                while (true) {
                    System.out.println("Child thread is executing ...");
                    if (main_thread.isAlive()) {
                        System.out.println("Main thread is still alive!");
                    } else {
                        System.out.println("Main thread is already dead!");
                    }
                }
            }
        };
        // if non-daemon thread stop, then daemon thread stop
        // if child_thread is not daemon thread, it continue running after main thread stop running
        // if child thread is daemon thread, it stop running after main thread stop running
        child_thread.setDaemon(true);
        child_thread.start();
        System.out.println("Main thread is about to end.");
    }
}