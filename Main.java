class AssignValToX extends Thread {
    private int x;

    public AssignValToX(int x) {
        this.x = x;
    }

    public void run() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Main.x = this.x;
//        System.out.println("x = " + this.x);
    }
}

class AssignXToY extends Thread {
    public void run() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Main.y = Main.x;
//        System.out.println("y = x " + Main.y);
    }
}

/*
* Example 1
* Assume that integer x is initially 0.
* Thread 1      Thread 2       Thread 3
* (1) x = 1;    (2) x = 2;     (3) y = x;
* What is the final value of y?
* (1) (2) (3) --> y is 0
* (2) (1) (3) --> y is 1
* (3) (2) (1) --> y is 2
* */
public class Main {

    public static int x = 0;
    public static int y;

    public static void main(String[] args) throws InterruptedException {

        var thread1 = new AssignValToX(1);
        var thread2 = new AssignValToX(2);
        var thread3 = new AssignXToY();

        thread1.start();
        thread2.start();
        thread3.start();

        thread1.join();
        thread2.join();
        thread3.join();

        System.out.println(y);


    }
}
