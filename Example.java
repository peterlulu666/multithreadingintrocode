class Addyz extends Thread {

    public void run() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Example.x = Example.y + Example.z;
    }
}

class Assignyz extends Thread {
    public void run() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Example.y = 1;
        Example.z = 2;
    }
}

/*
* Example 2
* Assume that y and z are initially 0.
* Thread 1              Thread 2
* x = y + z;            y = 1
*                       z = 2
* What is the final value of x?
* Thread 1              Thread 2
* (1) load r1, y 1      (4) assign y
* (2) add r1, z         (5) assign z, 2
* (3) store r1, x
* (1), (2), (3), (4), (5) --> x is 0
* (4), (1), (2), (3), (5) --> x is 1
* (1), (4), (5), (2), (3) --> x is 2
* (4), (5), (1), (2), (3) --> x is 3
* */
public class Example {

    public static int x;
    public static int y = 0;
    public static int z = 0;

    public static void main(String[] args) throws InterruptedException {

        var thread1 = new Addyz();
        var thread2 = new Assignyz();

        thread1.start();
        thread2.start();

        thread1.join();
        thread2.join();

        System.out.println(x);


    }
}
