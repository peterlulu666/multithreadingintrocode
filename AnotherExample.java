class AddOneToX extends Thread {

    public void run() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        AnotherExample.x = AnotherExample.x + 1;
    }
}

class AssignTwoToX extends Thread {
    public void run() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        AnotherExample.x = 2;
    }
}

/*
* Example 3
* Assume that the initial value of x is 0.
* Thread 1              Thread 2
* x = x + 1;            x = 2;
* What is the final value of x?
* Thread 1              Thread 2
* (1) load r1, x        (4) assign x, 2
* (2) add r1, 1
* (3) store r1, x
* (1), (2), (3), (4) --> x is 2
* (4), (1), (2), (3) --> x is 3
* (1), (2), (4), (3) -->  x is 1
* */
public class AnotherExample {

    public static int x = 0;

    public static void main(String[] args) throws InterruptedException {

        var thread1 = new AddOneToX();
        var thread2 = new AssignTwoToX();

        thread1.start();
        thread2.start();

        thread1.join();
        thread2.join();

        System.out.println(x);


    }
}
